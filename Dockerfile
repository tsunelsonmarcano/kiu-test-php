# Base image
FROM php:7.4-cli

# Install extensions
RUN docker-php-ext-install pdo pdo_mysql

# Set the working directory
WORKDIR /var/www/html

# Copy project files
COPY . .

# Expose port
EXPOSE 8000

# Run PHP CLI server
# CMD ["php", "-S", "127.0.0.1:8000"]