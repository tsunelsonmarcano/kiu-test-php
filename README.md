Para correr el proyecto basta con ejecutar php -S localhost:8000.

Si estás usando docker, solo basta correr el comando docker-compose up y consumir la api en http://localhost:8000

Se debe crear la base de datos con el archivo kiu.sql

Para configurar los parámetros de la base de datos, se debe realizar en la ruta app/config.php y en el archivo docker-compose.yml

La colección de POSTMAN para probar la API está en la raíz de este proyecto

Este desarrollo se hizo con php versión 7.4