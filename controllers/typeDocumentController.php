<?php

class typeDocumentController extends Controller
{
    private $_typeDocument;

    public function __construct()
    {
        parent::__construct();
        $this->_typeDocument = $this->loadModel('typeDocument');
    }

    /**
     * Show list document types
     */
    public function index()
    {
        $typeDocuments = $this->_typeDocument->findAll();

        if (!$typeDocuments) {
            http_response_code(204);
            return $this->responseJson(null);
        }

        return $this->responseJson($typeDocuments);
    }

    /**
     * Store new document types
     */
    public function store()
    {
        if ($name = $this->post('name')) {
            $typeDocument = $this->_typeDocument->save($name);
            http_response_code(201);
            return $this->responseJson($typeDocument);
        }

        http_response_code(422);
        return $this->responseJson(['error' => 'Debe enviar todo los parámetros requeridos']);
    }

    /**
     * Show document types
     */
    public function show()
    {
        $args = $this->_request->getArgs();

        if (isset($args[0])) {
            $id = $args[0];
            $typeDocument = $this->_typeDocument->find($id);

            if (!$typeDocument) {
                http_response_code(404);
                return $this->responseJson(['error' => 'Objeto no localizado']);
            }

            return $this->responseJson($typeDocument);
        }

        http_response_code(422);
        return $this->responseJson(['error' => 'Debe enviar un id válido']);
    }

    /**
     * Update document types
     */
    public function update()
    {
        $args = $this->_request->getArgs();
        $name = $this->post('name');

        if ($name && isset($args[0])) {
            $id = $args[0];

            if (!$this->_typeDocument->find($id)) {
                http_response_code(404);
                return $this->responseJson(['error' => 'Objeto no localizado']);
            }

            $typeDocument = $this->_typeDocument->update($id, $name);
            return $this->responseJson($typeDocument);
        }

        http_response_code(422);
        return $this->responseJson(['error' => 'Debe enviar todos los parámetros requeridos']);
    }

    /**
     * Remove document types
     */
    public function destroy()
    {
        $args = $this->_request->getArgs();

        if (isset($args[0])) {
            $id = $args[0];

            if (!$this->_typeDocument->find($id)) {
                http_response_code(404);
                return $this->responseJson(['error' => 'Objeto no localizado']);
            }

            if ($this->_typeDocument->existsInCustomers($id)) {
                http_response_code(422);
                return $this->responseJson(['error' => 'No se puede eliminar porque el tipo de documento contiene clientes asociados']);
            }

            $typeDocument = $this->_typeDocument->delete($id);
            http_response_code(204);
            return $this->responseJson(null);
        }

        http_response_code(422);
        return $this->responseJson(['error' => 'Debe enviar un id válido']);
    }
}