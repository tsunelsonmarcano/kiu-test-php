<?php

class customerController extends Controller
{
    private $_customer;
    private $_typeDocument;

    public function __construct()
    {
        parent::__construct();
        $this->_customer = $this->loadModel('customer');
        $this->_typeDocument = $this->loadModel('typeDocument');
    }

    /**
     * Show list customers
     */
    public function index()
    {
        $sorting = $this->get('sorting');

        if ($sorting) {
            $sorting = explode('.', $sorting);
            $sortingAttributes = ['id', 'name', 'last_name', 'type_document_id', 'document', 'created_at', 'updated_at'];
            
            if (count($sorting) === 2 && 
                (strtolower($sorting[1]) === 'desc' || 
                strtolower($sorting[1])  === 'asc') &&
                in_array(strtolower($sorting[0]), $sortingAttributes))  {
                $sorting = [
                    'column' => $sorting[0],
                    'direction' => $sorting[1],
                ];
            } else {
                $sorting = null;
            }
        }

        $customers = $this->_customer->findAll($sorting);

        if (!$customers) {
            http_response_code(204);
            return $this->responseJson(null);
        }

        return $this->responseJson($customers);
    }

    /**
     * Store new customer
     */
    public function store()
    {
        $name = $this->post('name');
        $lastName = $this->post('last_name');
        $typeDocumentId = $this->post('type_document_id');
        $document = $this->post('document');
        
        if ($name && $lastName && $typeDocumentId && $document) {

            $typeDocument = $this->_typeDocument->find($typeDocumentId);

            if (!$typeDocument) {
                http_response_code(404);
                return $this->responseJson(['error' => 'Objeto no localizado']);
            }

            $customer = $this->_customer->save($name, $lastName, $typeDocumentId, $document);
            http_response_code(201);
            return $this->responseJson($customer);
        }

        http_response_code(422);
        return $this->responseJson(['error' => 'Debe enviar todo los parámetros requeridos']);
    }

    /**
     * Show one customer
     */
    public function show()
    {
        $args = $this->_request->getArgs();

        if (isset($args[0])) {
            $id = $args[0];
            $customer = $this->_customer->find($id);

            if (!$customer) {
                http_response_code(404);
                return $this->responseJson(['error' => 'Objeto no localizado']);
            }

            http_response_code(200);
            return $this->responseJson($customer);
        }

        http_response_code(422);
        return $this->responseJson(['error' => 'Debe enviar un id válido']);
    }

    /**
     * Update customer
     */
    public function update()
    {
        $args = $this->_request->getArgs();
        $name = $this->post('name');
        $lastName = $this->post('last_name');
        $typeDocumentId = $this->post('type_document_id');
        $document = $this->post('document');

        if ($name && $lastName && $typeDocumentId && $document && isset($args[0])) {
            $id = $args[0];

            if (!$this->_customer->find($id)) {
                http_response_code(404);
                return $this->responseJson(['error' => 'Objeto no localizado']);
            }

            $typeDocument = $this->_typeDocument->find($typeDocumentId);

            if (!$typeDocument) {
                http_response_code(404);
                return $this->responseJson(['error' => 'Objeto no localizado']);
            }

            $customer = $this->_customer
                ->update($id, $name, $lastName, $typeDocumentId, $document);
            
            http_response_code(200);
            return $this->responseJson($customer);
        }

        http_response_code(422);
        return $this->responseJson(['error' => 'Debe enviar todos los parámetros requeridos']);
    }

    /**
     * Remove customer
     */
    public function destroy()
    {
        $args = $this->_request->getArgs();

        if (isset($args[0])) {
            $id = $args[0];

            if (!$this->_customer->find($id)) {
                http_response_code(404);
                return $this->responseJson(['error' => 'Objeto no localizado']);
            }

            $customer = $this->_customer->delete($id);
            http_response_code(204);
            return $this->responseJson(null);
        }

        http_response_code(422);
        return $this->responseJson(['error' => 'Debe enviar un id válido']);
    }
}