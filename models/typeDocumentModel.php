<?php

class typeDocumentModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function findAll()
    {
        $typeDocuments = $this->_db->query("SELECT * FROM type_documents");
        return $typeDocuments->fetchAll(PDO::FETCH_ASSOC);
    }

    public function find($id)
    {
        $typeDocument = $this->_db
            ->prepare("SELECT * FROM type_documents WHERE id = :id");
        $typeDocument->execute([
                ':id' => $id,
            ]);

        return $typeDocument->fetch(PDO::FETCH_ASSOC);
    }

    public function save($name)
    {
        try {
            $currentDate = date('Y-m-d H:i:s');
            $query = "INSERT INTO type_documents(`name`, `created_at`, `updated_at`) VALUES(:name, :created_at, :updated_at)";
            $this->_db
                ->prepare($query)
                ->execute([
                    ':name' => $name,
                    ':created_at' => $currentDate,
                    ':updated_at' => $currentDate,
                ]);

            return [
                'id' => $this->_db->lastInsertId(),
                'name' => $name,
                'created_at' => $currentDate,
                'updated_at' => $currentDate,
            ];
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function update($id, $name)
    {
        try {
            $currentDate = date('Y-m-d H:i:s');
            $query = "
                UPDATE type_documents
                SET `name` = :name,
                    updated_at = :updated_at
                WHERE id = :id
            ";

            $this->_db
                ->prepare($query)
                ->execute([
                    ':id' => $id,
                    ':name' => $name,
                    ':updated_at' => $currentDate,
                ]);

            return $this->find($id);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function delete($id)
    {
        try {
            $query = "
                DELETE FROM type_documents
                WHERE id = :id
            ";
            
            $this->_db
                ->prepare($query)
                ->execute([
                    ':id' => $id,
                ]);

        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function existsInCustomers($id)
    {
        $query = "
            SELECT id
            FROM customers
            WHERE type_document_id = :id
            LIMIT 1
        ";

        $customer = $this->_db
            ->prepare($query);
        $customer->execute([
                ':id' => $id,
            ]);

        return $customer->fetch(PDO::FETCH_ASSOC);
    }
}