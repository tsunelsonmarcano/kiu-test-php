<?php

class customerModel extends Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function findAll($sorting = null)
    {
        $query = "
            SELECT 
                customers.*,  
                type_documents.name AS type_document
            FROM customers
            INNER JOIN type_documents ON type_documents.id = customers.type_document_id
        ";

        if ($sorting && isset($sorting['column']) && isset($sorting['direction'])) {
            $query .=  " ORDER BY {$sorting['column']} {$sorting['direction']}";
        }

        $customers = $this->_db->query($query);
        return $customers->fetchAll(PDO::FETCH_ASSOC);
    }

    public function find($id)
    {
        $query = "
            SELECT 
                customers.*,
                type_documents.name AS type_document
            FROM customers
            INNER JOIN type_documents ON type_documents.id = customers.type_document_id
            WHERE customers.id = :id
        ";

        $customer = $this->_db
            ->prepare($query);
        $customer->execute([
                ':id' => $id,
            ]);

        return $customer->fetch(PDO::FETCH_ASSOC);
    }

    public function save($name, $lastName, $typeDocumentId, $document)
    {
        try {
            $currentDate = date('Y-m-d H:i:s');
            
            $query = "
                INSERT INTO customers(
                    `name`, 
                    `last_name`, 
                    `type_document_id`, 
                    `document`, 
                    `created_at`, 
                    `updated_at`
                ) 
                VALUES(
                    :name, 
                    :last_name, 
                    :type_document_id, 
                    :document, 
                    :created_at, 
                    :updated_at
                )
            ";

            $this->_db
                ->prepare($query)
                ->execute([
                    ':name' => $name,
                    ':last_name' => $lastName,
                    ':type_document_id' => $typeDocumentId,
                    ':document' => $document,
                    ':created_at' => $currentDate,
                    ':updated_at' => $currentDate,
                ]);

            return [
                'id' => $this->_db->lastInsertId(),
                'name' => $name,
                'last_name' => $lastName,
                'type_document_id' => $typeDocumentId,
                'document' => $document,
                'created_at' => $currentDate,
                'updated_at' => $currentDate,
            ];
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function update($id, $name, $lastName, $typeDocumentId, $document)
    {
        try {
            $currentDate = date('Y-m-d H:i:s');
            $query = "
                UPDATE customers
                SET `name` = :name, 
                    `last_name` = :last_name, 
                    `type_document_id` = :type_document_id, 
                    `document` = :document,
                    `updated_at` = :updated_at
                WHERE id = :id
            ";

            $this->_db
                ->prepare($query)
                ->execute([
                    ':id' => $id,
                    ':name' => $name,
                    ':last_name' => $lastName,
                    ':type_document_id' => $typeDocumentId,
                    ':document' => $document,
                    ':updated_at' => $currentDate,
                ]);

            return $this->find($id);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function delete($id)
    {
        try {
            $query = "
                DELETE FROM customers
                WHERE id = :id
            ";
            
            $this->_db
                ->prepare($query)
                ->execute([
                    ':id' => $id,
                ]);
                
        } catch (\Exception $e) {
            throw $e;
        }
    }
}