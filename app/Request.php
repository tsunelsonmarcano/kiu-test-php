<?php

class Request
{
    private $_controller;
    private $_method;
    private $_args;

    public function __construct()
    {
        if (isset($_SERVER['REQUEST_URI'])) {
            // Se captura la url
            // y se le quita el primer slash de la izquierda
            $url = ltrim($_SERVER['REQUEST_URI'], '/');
            // Se crea un array a partir 
            // de la división de la url
            // por los slash que contenga
            $url = explode('/', $url);
            $url = array_filter($url);

            // El primer elemento del array de la url (controlador)
            $this->_controller = strtolower(array_shift($url));
            // El segundo elemento del array de la url (método)
            $this->_method = strtolower(array_shift($url));

            // Lo que queda del resto de la url son los argumentos
            $this->_args = $url;
        }

        if (!$this->_controller) {
            // De no haber encontrado un contralor
            // se coloca el controlador por defecto
            $this->_controller = DEFAULT_CONTROLLER;
        }

        // Se captura el verbo (GET, POST, PUT, DELETE)
        $method = $_SERVER['REQUEST_METHOD'];
        // Se vuelve a captura la url
        $completeUrl = $_SERVER['REQUEST_URI'];

        // Si consigue un contrador en la url enviada
        // Que incluya como método un número
        if (preg_match('/\/' . $this->_controller . '\/[1-9]/', $completeUrl)) {
            // Si es GET, se llama al show
            if ($method === 'GET') {
                $this->_method = 'show';
            }

            // Si es PUT, se llama al update
            if ($method === 'PUT') {
                $this->_method = 'update';
            }

            // Si es DELETE, se llama al destroy
            if ($method === 'DELETE') {
                $this->_method = 'destroy';
            }

            // Se quita el primer slash de la url
            $url = ltrim($completeUrl, '/');
            // Se divide la url por slashes en un array
            $url = explode('/', $url);
            // Se deja solo los argumentos
            array_shift($url);

            $this->_args = $url;
        } else {
            // Si no consigue un número como método de la url

            // Si el verbo es POST, se llama al método store
            if ($method === 'POST' && !$this->_method) {
                $this->_method = 'store';
            }

            // si el verbo es GET, se llama alm étodo index
            if ($method === 'GET' && !$this->_method) {
                $this->_method = 'index';
            }

            // Si no llega a existir ningún método, se llama
            // al método index por defecto
            if (!$this->_method) {
                $this->_method = 'index';
            }
    
            // Si no existes argumentos, se le asgina un array vacío
            if (!isset($this->_args)) {
                $this->_args = [];
            }
        }
    }

    /**
     * Retornamos el valor del controlador
     */
    public function getController()
    {
        return $this->_controller;
    }

    /**
     * Retornamos el valor del método
     */
    public function getMethod()
    {
        return $this->_method;
    }

    /**
     * Retornamos el valor de los argumentos
     */
    public function getArgs()
    {
        return $this->_args;
    }
}