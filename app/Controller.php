<?php

abstract class Controller
{
    protected $_resquest;

    public function __construct()
    {
        $this->_request = new Request();
    }

    /**
     * Método para cargar los modelos
     * @param string $model
     * @return Model|Exception
     */
    public function loadModel($model)
    {
        $model .= 'Model';
        $route = ROOT . 'models' . DS . $model . '.php';

        if (is_readable($route)) {
            require_once $route;
            $model = new $model;
            return $model;
        } else {
            throw new \Exception("Error Processing Model");
            
        }
    }

    public function post($key)
    {      
        $json = file_get_contents('php://input');
        $data = json_decode($json, true);

        if (isset($data[$key]) && !empty($data[$key])) {
            $data[$key] = strip_tags($data[$key]);
            return trim($data[$key]);
        }
    }

    public function get($key)
    {      
        if (isset($_GET[$key]) && !empty($_GET[$key])) {
            $_GET[$key] = strip_tags($_GET[$key]);
            return trim($_GET[$key]);
        }
    }

    public function postAll()
    {      
        $json = file_get_contents('php://input');
        return $json;
    }

    public function responseJson($data)
    {      
        if ($data && !empty($data) && is_array($data)) {
            header('Content-Type: application/json');
            echo json_encode(compact('data'));
        }
    }

    abstract public function index();
    abstract public function store();
    abstract public function show();
    abstract public function update();
    abstract public function destroy();


}