<?php

class Bootstrap
{
    /**
     * En run, capturamos el controlador 
     * */ 
    public static function run(Request $request)
    {
        // Se divide el controlador de los parámtros urls enviados
        $controller = explode('?', $request->getController());
        $controller = $controller[0] . 'Controller';
        $routeController = ROOT . 'controllers' . DS . $controller . '.php';
        // Método del controlador
        $method = $request->getMethod();
        // Argumentos enviados
        $args = $request->getArgs();
        // Si la ruta existe
        if (is_readable($routeController)) {
            // Se incluye el archvo
            require_once $routeController;
            // Se instancia el contrador
            $controller = new $controller;

            // si el método enviado no existe en el controlador,
            // se usa el método por defecto index
            if (!method_exists($controller, $method)) {
                $method = 'index';
            }

            if (isset($args)) {
                // Si existen los argumentos
                // se le pasan al método del controlador
                call_user_func_array([$controller, $method], $args);
            } else {
                // Si no exiten los argumentos, solo se llama
                // el controlador con su método
                call_user_func([$controller, $method]);
            }

        } else {
            throw new \Exception("Error Processing Controller");
        }
    }
}