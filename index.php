<?php
// Definimos las constantes principales del sistema
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', realpath(dirname(__FILE__)) . DS);
define('APP_PATH', ROOT . 'app' . DS);

try {
    // Incluimos los archivos de app
    require_once APP_PATH . 'config.php';
    require_once APP_PATH . 'Request.php';
    require_once APP_PATH . 'Bootstrap.php';
    require_once APP_PATH . 'Controller.php';
    require_once APP_PATH . 'Model.php';
    require_once APP_PATH . 'Database.php';

    // Pivote a partir de Bootstrap run, al 
    // cual se le pasa el request 
    Bootstrap::run(new Request());

} catch (\Exception $e) {
    echo $e->getMessage();
}